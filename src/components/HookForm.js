import React, { useState } from 'react'
import axios from 'axios';

const HookForm  = () => {
    const [values, setValues] = useState({name: '', price: ''})
    
    const handleChange = e => {
        const {name, value} = e.target
        setValues({...values, [name]: value})
    }    

    async function handleSubmit(event) {
        event.preventDefault();
        await axios.post(
            'http://localhost:1234/rfp/create',
            {  name: `${values.name}`, price: `${values.price}` }
        );
       
        
    }

   
        return (
            <div style={{paddingLeft: '1vw',}}>
                
              <form onSubmit={handleSubmit}>
                <div style={{paddingTop: '2vh', paddingBottom: '2vh',}}>            
                  <label>Name:</label><br/>
                  <input
                    name="name"
                    onChange={handleChange}
                    value={values.name}
                  />
                </div>
                <div style={{paddingBottom: '2vh',}}>
                  <label>Price:</label><br/>
                  <input
                    name="price"
                    type="number"
                    onChange={handleChange}
                    value={values.price}
                  />
                </div>
              

                <button type="submit">Send</button>
              </form>
              </div>
        );
    
    }
        

export default HookForm;