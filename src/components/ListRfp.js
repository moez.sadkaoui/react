import React ,{Component} from 'react';
import axios from 'axios'
import './table.css'

export class ListRfp extends Component {
    constructor(props) {
        super(props)
    
        this.state={
           rfps: []
        }
    }

    componentDidMount(){
        axios.get('http://localhost:1234/rfp/get-all')
        .then(response => {
            this.setState({rfps: response.data})
        })
    }
deleteRfp(rfpId){
axios.delete('http://localhost:1234/rfp/'+rfpId+'/delete')
console.log(rfpId)
this.componentDidMount()
}

    render() {
        const {rfps} = this.state
        return ( 
            <div >
list of Rfp:
<div className="ReactTable"  >



{
    rfps.length ?
rfps.map(rfp => <div className="todo-list"  key={rfp._id} >titre: {rfp.name} prix par jour: {rfp.price} <button  onClick={()=> this.deleteRfp(rfp._id)}  variant="danger">delete</button></div>):
null
}
</div>
            </div>
        )
    }
}

export default ListRfp