import React, { Component} from 'react'
import axios from 'axios'
import ListRfp from './ListRfp'

export class Form extends Component {
constructor(props) {
    super(props)

    this.state={
        name : '',
        price : ''
    }
}

 
changeHandler = (e) => {
    this.setState({[e.target.name]: e.target.value})
}

submitHandler = e => {
    
   e.preventDefault();
    axios.post('http://localhost:1234/rfp/create',this.state)
  
}


    render() {
       
        const {name,price} = this.state
        return(
            <form onSubmit={this.submitHandler}>
                <div>
                    <label>Titre</label>
                    <input type='text' name="name"value= {name} onChange={this.changeHandler}/>
                </div>
                <div>
                    <label>Prix</label>
                    <input type='text' name="price" value= {price} onChange={this.changeHandler}/>
                </div>
                <button type="submit">Ajouter</button>
            </form>
        )
    }
}