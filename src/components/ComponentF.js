import React ,{useContext}from 'react'
import { CountContext, UserContext } from '../App'
  
     /*   <div>
          <UserContext.Consumer>
              {
                  user => {
                      return(
                      <ChannelContext.Consumer>
                          {channel => {
                              return <div>
                              user context value {user}, channel Context value {channel}
                          </div>
                          }
                          }
                      </ChannelContext.Consumer>) 
                      
                  
              }
            }
          </UserContext.Consumer>
        </div>*/
  


function ComponentF() {
    const countContext = useContext(CountContext)
    return(
        <div>
            ComponentF - {countContext.countState}
 <button onClick={() => countContext.countDispatch('increment')}>Increment</button>
            <button onClick={() => countContext.countDispatch('decrement')}>Decrement</button>
            <button onClick={() => countContext.countDispatch('reset')}>Reset</button>
        </div>
    )
}


export default ComponentF