import React , {useReducer}from 'react';

import './App.css';
import { Form } from './components/Form';
import {ListRfp} from './components/ListRfp';
import DataFetching from './components/DataFetching'
import ComponentC from './components/ComponentC'
import CounterOne from './components/CounterOne';
import CounterTwo from './components/CounterTwo';
import ComponentB from './components/ComponentB';
import ComponentA from './components/ComponentA';
import DataFetchingTwo from './components/DataFetchingTwo'
import ParentComponent from './components/PrentComponent';
import Counter from './components/Counter';
import FocusInput from './components/FocusInput';
import ClassTimer from './components/ClassTimer';
import HookTimer from './components/HookTimer';
import {Route,BrowserRouter as Router,Link,Switch,NavLink, BrowserRouter,Redirect} from "react-router-dom"
import {browserHistory} from "@version/react-router-v3";
import Redirection from './components/redirection'
import HookForm from './components/HookForm'


export const CountContext = React.createContext();
export const UserContext = React.createContext();
export const ChannelContext = React.createContext();
/* <UserContext.Provider  value={'Vishwas'}>
    <ChannelContext.Provider value={'CodeVolution'}>

    <ComponentC />
    </ChannelContext.Provider>
   
   </UserContext.Provider>*/

   const initialState = 0
const reducer = (state, action) => {
switch(action){
    case 'increment':
        return  state + 1
        case 'decrement':
        return state - 1
        case 'reset':
            return initialState
            default:
                return state
}
}


function App() {

 const [count, dispatch] = useReducer(reducer,initialState)
  return (
 /*   <CountContext.Provider value={{countState: count,countDispatch: dispatch}}>
    <div className="App">
      Count - {count}
   <ComponentA />
   <ComponentB />
   <ComponentC />
   
  
    </div>
    </CountContext.Provider>*/
    <DataFetching />
    
   /* <Router history={browserHistory}>
 <div className="App">
      <nav>
        <ul>
          <li>
            <NavLink to="/counter-two">two</NavLink>
          </li>
      
          <li>
            <Link to="/counter-one">one</Link>
          </li>
        </ul>
      </nav>
     
    </div>
    <Route path="/counter-two" component={CounterTwo}/>
    <Route path="/counter-one" component={CounterOne} render={()=>(<Redirect to='/counter-one' />)}/>
  
    </Router>*/
   
  );
}

export default App;
